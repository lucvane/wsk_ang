import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Call } from './call'
import { Subject, Observable } from "rxjs"
import * as io from "socket.io-client"


@Injectable({
  providedIn: 'root'
})
export class CallService {


  readonly STATUS_ANSWERED = "ANSWERED"
  readonly STATUS_FAILED = "FAILED"
  readonly STATUS_NO_ANSWER = "NO ANSWER"
  readonly STATUS_BUSY = "BUSY"

  private socket = io("http://localhost:3000")
  private apiUrl: string = 'http://localhost:3000'
  private callId = new Subject<number>()
  private callStatus = new Subject<string>()

  constructor(private http: HttpClient) {
    this.socket.on("status", status => {
      this.callStatus.next(status)
      })
   }

  placeCall(number: string) {
    const postData = { number1: '999999999', number2: number }
    this.http.post<Call>(this.apiUrl + '/call', postData).subscribe(data => {
      this.callId.next( data.id);
    });
  }

  checkStatus(callId) {
    this.http.get<Call>(this.apiUrl + '/status/' + callId)
      .subscribe(data => {
        this.callStatus.next(data.status);
      });
  }

  getCallId(): Observable<number> {
    return this.callId.asObservable()
  }

  getCallStatus(): Observable<string> {
    return this.callStatus.asObservable()
  }


}
