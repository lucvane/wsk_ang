import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {

  number: string
  validator = /(^[0-9]{9}$)/
  state: string = "waiting"
  interval: number

  constructor(private callService: CallService) { }

  ngOnInit() {
  }

  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.state = "ringing"
    } else {
      console.info('Numer niepoprawny')
    }
  }

  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }

  checkStatus() {
    this.interval = setInterval(() => {
      this.callService.checkStatus()
      let state = this.callService.getCallStatus()
      if (this.state !== state) {
        this.state = state
      }
      if (
        [
          this.callService.STATUS_ANSWERED,
          this.callService.STATUS_FAILED,
          this.callService.STATUS_NO_ANSWER,
          this.callService.STATUS_BUSY,
        ].indexOf(state) !== -1
      ) {
        clearInterval(this.interval)
      }
    }, 500)

  }
}
