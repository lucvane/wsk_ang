import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Call } from './call'

@Injectable({
  providedIn: 'root'
})
export class CallService {

  readonly STATUS_ANSWERED = "ANSWERED"
  readonly STATUS_FAILED = "FAILED"
  readonly STATUS_NO_ANSWER = "NO ANSWER"
  readonly STATUS_BUSY = "BUSY"

  private apiUrl: string = 'http://localhost:3000'
  private callId: number = null
  private callStatus: string = null;

  constructor(private http: HttpClient) { }

  placeCall(number: string) {
    const postData = { number1: '999999999', number2: number }
    this.http.post<Call>(this.apiUrl + '/call', postData).subscribe(data => {
      this.callId = data.id
    });
  }

  checkStatus() {
    this.http.get<Call>(this.apiUrl + '/status/' + this.callId)
      .subscribe(data => {
        this.callStatus =data.status;
      });
  }

  getCallStatus() {
    return this.callStatus
  }


}
