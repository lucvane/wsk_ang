const express = require("express")
const app = express()
const Dialer = require("dialer").Dialer
const cors = require("cors")
const bodyParser = require("body-parser")
const http = require("http").Server(app)
const io = require("socket.io")(http)

const config = {
    url: "https://uni-call.fcc-online.pl",
    login: 'focus05',
    password: '7654yhgfhn'
}

app.use(cors())
app.use(bodyParser.json())

Dialer.configure(config)

http.listen(3000, () => {
    console.log("app listening on port 3000")
})

io.on("connection", socket => {
    console.log("user connected")
    socket.on("disconnect", () => {
        console.log("user disconnected")
    })
    socket.on("message", message => {
        console.log("message", message)
    })
    socket.on("status", status => {
        console.log("status", status)
    })
    io.emit("message", "connected!")
})

let bridges = []
let currentStatus
let callsId = 0

app.post("/call", async (req, res) => {
    const body = req.body
    callsId++
    try {
        console.info("callsId: %s", callsId)
        console.info(body.number1, body.number2)
        bridges[callsId] = await Dialer.call(body.number1, body.number2)
    } catch (error) {
        res.json({
            succes: false,
            error: error,
        })
        return
    }

    let interval = setInterval(async () => {
        let status
        try {
            status = await bridges[callsId].getStatus()
        } catch (error) {
            status = "FAILED"
        }
        if (currentStatus !== status) {
            currentStatus = status
            io.emit("status", status)
        }

        if (
            currentStatus === "ANSWERED" ||
            currentStatus === "FAILED" ||
            currentStatus === "BUSY" ||
            currentStatus === "NO ANSWER"
        ) {
            clearInterval(interval)
        }
    }, 500)

    res.json({
        id: callsId,
        success: true,
    })
})

app.get("/status/:callsId", async (req, res) => {
    let callsId = req.params.callsId
    let status
    if (callsId in bridges) {
        status = await bridges[callsId].getStatus()
        res.json({
            success: true,
            status: status,
        })
    } else {
        res.json({
            success: true,
            status: "wrong calls id",
        })
    }
})